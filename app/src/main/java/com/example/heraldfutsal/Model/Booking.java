package com.example.heraldfutsal.Model;


public class Booking{
    private String date;
    private String time;
    private String username;
    private String userId;
    private String mobilenumber;
    private boolean paid;

    public Booking(String date, String time) {
        this.date = date;
        this.time = time;
        this.paid = false;
    }

    public Booking(String date, String time, String username,String userId,String mobilenumber) {
        this.date = date;
        this.time = time;
        this.username = username;
        this.userId = userId;
        this.mobilenumber = mobilenumber;
        this.paid = false;
    }

    public Booking() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }
}
