package com.example.heraldfutsal.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.heraldfutsal.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class Reset_Password_Activity extends AppCompatActivity {

    EditText email_text;
    Button send_request;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        auth = FirebaseAuth.getInstance();

        email_text = (EditText) findViewById(R.id.email_reset_id);
        send_request = (Button) findViewById(R.id.send_request);


        send_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email = email_text.getText().toString();
//                resetPassWord(Email);
                Email = Email.trim();
                if (Email.isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Reset_Password_Activity.this);
                    builder.setMessage(R.string.login_reset_message)
                            .setTitle(R.string.login_error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();

                } else {
                    auth.sendPasswordResetEmail(Email)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getApplication(), "Reset password successful"+ "\n" +"Check email to reset yout password", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(Reset_Password_Activity.this, SignInActivity.class);
                                        startActivity(intent);
                                    } else {
                                        email_text.setError("Invalid Email");
                                        Toast.makeText(getApplication(), "invalid Email", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
    }
}