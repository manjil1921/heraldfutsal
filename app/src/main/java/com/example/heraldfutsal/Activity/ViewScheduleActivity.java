package com.example.heraldfutsal.Activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.heraldfutsal.DividerItemDecoration;
import com.example.heraldfutsal.Model.Booking;
import com.example.heraldfutsal.Model.User;
import com.example.heraldfutsal.Adapter.MyBookingAdapter;
import com.example.heraldfutsal.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ViewScheduleActivity extends AppCompatActivity {


    private DatabaseReference mDatabase;

    private List<Booking> myBookings = new ArrayList<>();
    private RecyclerView rView;
    private MyBookingAdapter mAdapter;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_schedule);

        //add to activity you want to pull variables from
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.user = (User) getIntent().getSerializableExtra("user");
        }

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setIcon(R.drawable.ic_action_calendar);
        actionBar.setTitle("My Schedule");

        mAdapter = new MyBookingAdapter(this.myBookings,new MyBookingAdapter.CancelButtonClickListener() {
            @Override
            public void buttonCancelClick(View v, int position) {

            }

            @Override
            public void buttonBookingOnClick(View v, int position) {
                Toast.makeText(ViewScheduleActivity.this, "\n" + "Cancelling successful!", Toast.LENGTH_SHORT).show();
                //Booking booking = bookingList.get(position);
                //showDialog(booking);

            }
        });



        // Initialize Firebase Reference
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("bookings").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    collectMyBooking(dataSnapshot);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //refreshBookingList();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("DB_ERROR", "The read failed: " + databaseError.getMessage());
            }
        });

        rView = (RecyclerView) findViewById(R.id.recycleView2);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rView.setLayoutManager(mLayoutManager);
        rView.setItemAnimator(new DefaultItemAnimator());
        // add line decoration
        rView.addItemDecoration(new DividerItemDecoration(this));

        // set adapter
        rView.setAdapter(mAdapter);

    }


    public void collectMyBooking(DataSnapshot bookingData) throws ParseException {

        Calendar c = Calendar.getInstance();

        // set the calendar to start of today
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        // and get that as a Date
        Date todayDate = c.getTime();
        Locale id = new Locale("en", "");

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyy", id);

        /*String todayDateString = sdf.format(todayDate);
        Log.d("LOCALE", Locale.getDefault().getDisplayLanguage());
        Log.d("TODAY_DATE", todayDateString);*/

        for (DataSnapshot date : bookingData.getChildren()) {

            Log.d("BOOKING_DATE", date.getKey());
            Date dates = sdf.parse(date.getKey());

            if (!dates.before(todayDate)) {
                for (DataSnapshot hour : date.getChildren()) {
                    Log.d("USER_ID", hour.child("userId").getValue().toString());
                    if (user.getUserId().equals(hour.child("userId").getValue().toString())) {
                        Booking b = hour.getValue(Booking.class);
                        myBookings.add(b);
                    }
                }

            }
        }

        mAdapter.notifyDataSetChanged();
    }


}
