package com.example.heraldfutsal.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.heraldfutsal.Model.User;
import com.example.heraldfutsal.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class SignUpActivity extends AppCompatActivity {


    private EditText emailText,
            usernameText,
            passwordText,mnumber;

    private Button buttonCreate;
    private TextView txtAlreadysignIn;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mDatabase;

    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // Initialize FirebaseAuth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        emailText = (EditText) findViewById(R.id.emailField);
        usernameText = (EditText) findViewById(R.id.usernameField);
        passwordText = (EditText) findViewById(R.id.passwordField);
        buttonCreate = (Button) findViewById(R.id.btnCreate);
        txtAlreadysignIn = (TextView) findViewById(R.id.txtSigin);
        mnumber = (EditText) findViewById(R.id.number1);
        createAccount();
        alreadySignIn();
    }

    public void createAccount(){
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String str_username = usernameText.getText().toString();
                String str_email = emailText.getText().toString();
                String str_number = mnumber.getText().toString();
                String str_password = passwordText.getText().toString();

                if (TextUtils.isEmpty(str_username) || TextUtils.isEmpty(str_email) || TextUtils.isEmpty(str_number) || TextUtils.isEmpty(str_password)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                    builder.setMessage(R.string.signup_error_message)
                            .setTitle(R.string.signup_error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if(str_password.length() < 6){
                    Toast.makeText(SignUpActivity.this, "Password must have 6 characters!", Toast.LENGTH_SHORT).show();
                }
                else if(str_number.length() == 10){
                    Toast.makeText(SignUpActivity.this, "Please enter valid 10 digit phone number!", Toast.LENGTH_SHORT).show();
                } else {
                    register(str_username,str_email,str_number, str_password);
                }


            }
        });

    }


    public void register(final String username,final String email, final String mobilenumber, String password){
        mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = mFirebaseAuth.getCurrentUser();
                            String userID = firebaseUser.getUid();

                            mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(userID);
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("username", username.toLowerCase());
                            map.put("email",email);
                            map.put("mobilenumber", mobilenumber);
                            map.put("userId", userID);

                            pd = new ProgressDialog(SignUpActivity.this);
                            pd.setMessage("Loading");
                            pd.show();

                            mDatabase.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
//                                        pd.dismiss();
                                        Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }
                            });
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                            builder.setMessage(task.getException().getMessage())
                                    .setTitle(R.string.login_error_title)
                                    .setPositiveButton(android.R.string.ok, null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }
                });
    }


    public void alreadySignIn(){
        txtAlreadysignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });
    }

}


