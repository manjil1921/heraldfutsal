package com.example.heraldfutsal.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.heraldfutsal.DividerItemDecoration;
import com.example.heraldfutsal.Model.Booking;
import com.example.heraldfutsal.Model.User;
import com.example.heraldfutsal.R;
import com.example.heraldfutsal.Adapter.TimeAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class BookingActivity extends AppCompatActivity {



    private static final String TAG = "BOOKING_LIST";
    private DatabaseReference mDatabase;

    private List<Booking> bookingList = new ArrayList<>();
    private List<Booking> bookingListFromFirebase = new ArrayList<>();
    private RecyclerView rView;
    private TimeAdapter mAdapter;
    private String thisDate;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        //add to activity you want to pull variables from
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.thisDate = extras.getString("thisDate");
            this.user = (User) getIntent().getSerializableExtra("user");
        }

        final ActionBar actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_calendar);
        actionBar.setTitle(thisDate);

        // Initialize Firebase Reference
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("bookings").child(thisDate).addValueEventListener(new ValueEventListener() {
            public static final String TAG = "LIST_ADDED_FIREBASE";

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot bookingSnapshot : dataSnapshot.getChildren()){
                    Booking b = bookingSnapshot.getValue(Booking.class);
                    bookingListFromFirebase.add(b);
                    Log.i(TAG,"add booking hour = " + b.getTime());
                }
                refreshBookingList();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(TAG, "The read failed: " + databaseError.getMessage());
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });

        rView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new TimeAdapter(this.bookingList, new TimeAdapter.MyAdapterListener() {
            @Override
            public void buttonBookingOnClick(View v, int position) {
                Booking booking = bookingList.get(position);
                showDialog(booking);
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rView.setLayoutManager(mLayoutManager);
        rView.setItemAnimator(new DefaultItemAnimator());
        // add line decoration
        rView.addItemDecoration(new DividerItemDecoration(this));

        // set adapter
        rView.setAdapter(mAdapter);

        /*rView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Booking booking = bookingList.get(position);
                Toast.makeText(getApplicationContext(), booking.getDate() + " o'clock "
                        + booking.getSlotTime(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        initBookingList();
    }

    private void showDialog(final Booking b) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        //set title dialog
        alertDialogBuilder.setTitle("Booking Confirmation");

        //set message from the dialog

        alertDialogBuilder
                .setMessage("\n" + "You will be booking futsal court on" +"\n"+ b.getDate()
                        + " \n" +  b.getTime()+ "o'clock " + "?\n\n"
                        + "\n" + "Price will be 1000 Rs per hour"+ "\n\n"+ "So Do you want to confirm for booking?" )

                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id){
                        createBooking(b);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });


        // create a dialog alert from the builder

        AlertDialog alertDialog = alertDialogBuilder.create();

        //displays a dialog alert
        alertDialog.show();
    }

    private void createBooking(Booking b){

//        long deposit = user.getDeposit();
        b.setUsername(user.getUsername());
        b.setUserId(user.getUserId());
        b.setMobilenumber(user.getMobilenumber());

//        if(deposit < 500){
//                Toast.makeText(getApplicationContext(), "\n" + "Insufficient deposits!", Toast.LENGTH_SHORT).show();
//        } else {
//            deposit = deposit - 500;
//        user.setDeposit(deposit);
        mDatabase.child("bookings").child(b.getDate()).child(b.getTime()).setValue(b);
        mDatabase.child("users").child(user.getUserId()).setValue(user);
        Toast.makeText(getApplicationContext(), "\n" + "Booking successful!", Toast.LENGTH_SHORT).show();
    }
//    }

    private void initBookingList(){

        /**
         *
         * Add items to the dataset
         */
        Booking booking = new Booking(thisDate, "06:00 AM - 7:00 AM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "7:00 AM - 8:00 AM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "8:00 AM - 09:00 AM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "09:00 AM - 10:00 AM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "10:00 AM - 11:00 AM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "11:00 AM - 12:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "12:00 PM - 13:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "13:00 PM - 14:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "14:00 PM - 15:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "15:00 PM - 16:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "16:00 PM - 17:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "17:00 PM - 18:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "18:00 PM - 19:00 PM");
        bookingList.add(booking);

        booking = new Booking(thisDate, "19:00 PM - 20:00 PM");
        bookingList.add(booking);

//        booking = new Booking(thisDate, "23:00 - 24:00");
//        bookingList.add(booking);

        /*for(Booking bData : bookingList) {
            //Booking bData = bList;
            Log.i(TAG, "booking hour = " + bData.getSlotTime() + " | " + bData.getDate());
        }*/

        mAdapter.notifyDataSetChanged();
    }

    private void refreshBookingList(){
//
        for(Booking bData : bookingListFromFirebase) {
            //Booking bData = bList;
            Log.i(TAG, "booking hour = " + bData.getTime() + " | " + bData.getDate());
        }

        for(Booking bDataFireBase : bookingListFromFirebase){
            int index = 0;
            for(Booking bData : bookingList){

                Log.i(TAG,"booking hour = " + bData.getTime() + " | " + bData.getDate());

                if(bData.getTime().equals(bDataFireBase.getTime())){
                    bookingList.set(index, bDataFireBase);
                    Log.i(TAG,"\n" + "replace in the index = " + index + " | username = " + bDataFireBase.getUsername());
                }
                index++;
            }
        }

        mAdapter.notifyDataSetChanged();
    }

}
