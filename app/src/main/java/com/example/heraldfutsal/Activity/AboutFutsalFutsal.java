package com.example.heraldfutsal.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.example.heraldfutsal.ChildAnimationExample;
import com.example.heraldfutsal.R;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AboutFutsalFutsal extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        ImageSlider imageSlider=findViewById(R.id.slider);

        List<SlideModel> slideModels=new ArrayList<>();
        slideModels.add(new SlideModel(R.drawable.image1,"Date: 2076/12/01"));
        slideModels.add(new SlideModel(R.drawable.image2,"Date: 2076/12/03"));
        slideModels.add(new SlideModel(R.drawable.image3,"Date: 2076/12/05"));
        slideModels.add(new SlideModel(R.drawable.image4,"Date: 2076/12/07"));
        imageSlider.setImageList(slideModels,true);
    }
}
