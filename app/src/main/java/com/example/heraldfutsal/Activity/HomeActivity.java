package com.example.heraldfutsal.Activity;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.heraldfutsal.Model.User;
import com.example.heraldfutsal.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;
import com.stacktips.view.utils.CalendarUtils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity {


    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private String mUserId;
    private User mUser;
    private CustomCalendarView calendarView;
    Locale locale = new Locale("en");

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Locale.setDefault(locale);

        Log.d("TOken ", "" + FirebaseInstanceId.getInstance().getToken());
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            loadSignInView();
        } else {

            Toast.makeText(HomeActivity.this, "\n" +
                    "Welcome to the Herald Futsal App", Toast.LENGTH_SHORT).show();

            mUserId = mFirebaseUser.getUid();
            mDatabase.child("users").child(mUserId).addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // Get Post object and use the values to update the UI
                    mUser = dataSnapshot.getValue(User.class);
                    TextView hiUsernameText = (TextView) findViewById(R.id.hi_username);
                    hiUsernameText.setText("Hello" + "\n" + mUser.getUsername());

                    // ...
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    TextView hiUsernameText = (TextView) findViewById(R.id.hi_username);
                    hiUsernameText.setText("\n" + "Failed to read database: " + databaseError.getCode());
                    // ...
                }
            });

            //Initialize CustomCalendarView from layout
            calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);

            //Initialize calendar with date
            Calendar currentCalendar = Calendar.getInstance(locale);

            //Show Monday as first date of week
            calendarView.setFirstDayOfWeek(Calendar.MONDAY);

            //Show/hide overflow days of a month
            calendarView.setShowOverflowDate(false);

            //call refreshCalendar to update calendar the view
            calendarView.refreshCalendar(currentCalendar);

            calendarView.setCalendarListener(new CalendarListener() {
                @Override
                public void onDateSelected(Date date) {
                    if (!CalendarUtils.isPastDay(date)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM", locale);
                        Toast.makeText(HomeActivity.this, sdf.format(date), Toast.LENGTH_SHORT).show();
                        //selectedDateTv.setText(df.format(date));
                        sdf = new SimpleDateFormat("dd MMMM yyy", locale);
                        String selectedDate = sdf.format(date);
                        Intent intent = new Intent(HomeActivity.this, BookingActivity.class);
                        intent.putExtra("thisDate", selectedDate);
                        intent.putExtra("user", mUser);
                        startActivity(intent);
                    } else {
                        Toast.makeText(HomeActivity.this, "\n" +
                                "Please choose another day", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onMonthChanged(Date date) {
                    //SimpleDateFormat df = new SimpleDateFormat("MMM-yyyy");
                    //Toast.makeText(HomeActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
                }
            });

            //adding calendar day decorators
            List<DayDecorator> decorators = new ArrayList<>();
            decorators.add(new CalendarColorDecorator());
            calendarView.setDecorators(decorators);
            calendarView.refreshCalendar(currentCalendar);
            //touchable deposit card view
            CardView depositCardView = (CardView) findViewById(R.id.cardView2);
            depositCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    showDepositDialog();
                }
            });
        }
    }

//    private void showDepositDialog() {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//
//        //set title dialog
//        alertDialogBuilder.setTitle("Deposit");
//
//        //set pesan dari dialog
//        alertDialogBuilder
//                    .setMessage("\n" +
//                            "To be able to book a field schedule, you must have a deposit. " +
//                        "\n" +
//                            "Contact admin to add your deposit.")
//                .setIcon(R.mipmap.ic_launcher)
//                .setCancelable(false)
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id){
//                        dialog.dismiss();
//                    }
//                });
//
//        //create dialog alerts from the builder
//        AlertDialog alertDialog = alertDialogBuilder.create();
//
//        //
//displays a dialog alert
//        alertDialog.show();
//    }

    private void loadSignInView() {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_signout) {
                    mFirebaseAuth.signOut();
                    Toast.makeText(HomeActivity.this, "\n" +
                            "You have been logged out", Toast.LENGTH_LONG).show();
                    loadSignInView();
                    return true;
                } else if (id == R.id.action_about) {
                    Intent intent1 = new Intent(HomeActivity.this, AboutFutsalFutsal.class);
                    startActivity(intent1);
                } else if (id == R.id.menu_share) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBodyText = "Check it out. This is awesome app https://play.google.com/store/apps/details?id=" + getPackageName();
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "About Herald Futsal app");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                    startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
                    return true;

                } else if (id == R.id.action_view_schedule) {
                    Intent intent = new Intent(HomeActivity.this, ViewScheduleActivity.class);
                    intent.putExtra("user", mUser);
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
        popup.inflate(R.menu.menu_home);
        popup.show();
    }


    private class CalendarColorDecorator implements DayDecorator {

        public void decorate(DayView dayView) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dayView.getDate());

            if (CalendarUtils.isPastDay(dayView.getDate())) {
                int color = Color.parseColor("#CFD8DC");
                dayView.setTextColor(color);
            } else if (CalendarUtils.isToday(cal)) {
                int colorBG = Color.parseColor("#bef67a");
                dayView.setTypeface(null, Typeface.BOLD);
                dayView.setBackgroundColor(colorBG);
            }
        }
    }





//        @Override
//        public boolean onCreateOptionsMenu(Menu menu) {
//            getMenuInflater().inflate(R.menu.menu_home,menu);
//            return true;
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            switch(item.getItemId()){
//                case  R.id.action_signout:
//                    mFirebaseAuth.signOut();
//                    Toast.makeText(HomeActivity.this, "\n" +
//                            "You have been logged out", Toast.LENGTH_LONG).show();
//                    loadSignInView();
//                    break;
//
//                case R.id.action_about:
//                    Intent intent1 = new Intent(HomeActivity.this, AboutFutsalFutsal.class);
//                    startActivity(intent1);
//                    break;
//
//                case R.id.menu_share:
//                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                    sharingIntent.setType("text/plain");
//                    String shareBodyText = "Check it out. This is awesome app https://play.google.com/store/apps/details?id="+getPackageName();
//                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"About Herald Futsal app");
//                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
//                    startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
//                    break;
//
//                case R.id.action_view_schedule:
//                    Intent intent = new Intent(HomeActivity.this, ViewScheduleActivity.class);
//                    intent.putExtra("user", mUser);
//                    startActivity(intent);
//                    break;
//
//
//            }
//            return super.onOptionsItemSelected(item);
//        }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.home_image)
                    .setTitle("Close Application")
                    .setMessage("Do you want to Exit?")
                    .setNeutralButton("Rate Us", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                Uri marketUri = Uri.parse("market://details?id="+getPackageName());
                                Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                                startActivity(marketIntent);
                            }catch(ActivityNotFoundException e) {
                                Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                                Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                                startActivity(marketIntent);
                            }

                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            HomeActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        } else {
            HomeActivity.super.onBackPressed();
        }
    }


}