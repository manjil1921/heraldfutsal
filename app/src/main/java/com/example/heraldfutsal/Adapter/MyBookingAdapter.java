package com.example.heraldfutsal.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.heraldfutsal.Model.Booking;
import com.example.heraldfutsal.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;


public class MyBookingAdapter extends RecyclerView.Adapter<MyBookingAdapter.MyViewHolder>{

    private List<Booking> myBookings;

    public static CancelButtonClickListener onClicklistener;

    public interface CancelButtonClickListener {

        void buttonCancelClick(View v, int position);

        void buttonBookingOnClick(View v, int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView confirmDate, confirmTime;
        public Button cancelButton;

        public MyViewHolder(View view){
            super(view);
            confirmDate = (TextView) view.findViewById(R.id.textDateConfirm);
            confirmTime = (TextView) view.findViewById(R.id.textTimeConfirm);
            cancelButton = (Button) view.findViewById(R.id.buttonCancel);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistener.buttonCancelClick(v, getAdapterPosition());

                }
            });


        }
    }

    public MyBookingAdapter(List<Booking> myBookings,CancelButtonClickListener listener) {
        this.myBookings = myBookings;
        this.onClicklistener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_schedule_row, parent, false);

        return new MyBookingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Booking booking= myBookings.get(position);
        holder.confirmTime.setText(booking.getTime());
        holder.confirmDate.setText(booking.getDate());
    }

    @Override
    public int getItemCount() {
        return myBookings.size();
    }


}
