package com.example.heraldfutsal.Adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.heraldfutsal.Model.Booking;
import com.example.heraldfutsal.R;

import java.util.List;



public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.MyViewHolder>{

    private List<Booking> bookingList;
    public static MyAdapterListener onClickListener;

    public interface MyAdapterListener {

        void buttonBookingOnClick(View v, int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView slotTimeText, status;
        public Button buttonBooking;

        public MyViewHolder(View view) {
            super(view);
            slotTimeText = (TextView) view.findViewById(R.id.textTimeBook);
            status = (TextView) view.findViewById(R.id.textStatusBook);
            buttonBooking = (Button) view.findViewById(R.id.buttonBooking);

            buttonBooking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.buttonBookingOnClick(v, getAdapterPosition());
                }
            });

        }
    }

    public TimeAdapter(List<Booking> bookingList, MyAdapterListener listener){
        this.bookingList = bookingList;
        this.onClickListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bookingtimerow, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Booking booking= bookingList.get(position);
        holder.slotTimeText.setText(booking.getTime());
        if(booking.getUsername() == null){
            holder.status.setText("\n" + "Not yet booked");
            holder.status.setTextColor(Color.parseColor("#757575"));
            holder.buttonBooking.setAlpha(1f);
            holder.buttonBooking.setEnabled(true);
        } else {
            holder.status.setText("\n" + "Booked by" +"\n"+ booking.getUsername());
            holder.status.setTextColor(Color.parseColor("#689F38"));
            holder.buttonBooking.setAlpha(.25f);
            holder.buttonBooking.setEnabled(false);
        }

    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }
}
